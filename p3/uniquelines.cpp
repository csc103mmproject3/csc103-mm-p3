#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;

unsigned long countWords(const string& s, set<string>& wl);


unsigned long countLines()
	{
		return 1;
	}
	unsigned long countUnLines (const string& s, set<string>& wl)
	{
		wl.insert(s);
		return wl.size();
	}
int main()
{
	string s;
	unsigned long Lines = 0;
	unsigned long UnLines = 0;

	set<string> wl;

	while(getline(cin, s))
	{
		Lines += countLines();
		UnLines += countUnLines(s,wl);


	cout << Lines << "\t" << UnLines << "\t" << endl;

		UnLines = 0;
	}
	return 0;
}
