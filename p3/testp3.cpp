/*
 * CSc103 Project 3: wordcount++
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
#include<vector>
using std::vector;
using std::set;


//unsigned long countWords(const string& s, set<string>& wl);

unsigned long uniq_words(const string& s, set<string>& w)
{
string word = "";

  for(unsigned int i = 0; i < s.length();i++)
    {
      if(s.at(i) != ' ' && s.at(i) != '\n' && s.at(i) != '\t')
      {
        word += s.at(i);

      } else
      {
       if(word.length() > 0 )
       {
       w.insert(word);
       word = "";
       }
      }

    }
return w.size();
}

unsigned long characters(const string& a){
return a.length();
 }

unsigned long wcounter(const string& st, vector<string>& v) {
string word = "";

  for(unsigned int i = 0; i < st.length();i++)
    {
      if(st.at(i) != ' ' && st.at(i) != '\n' && st.at(i) != '\t')
      {
        word += st.at(i);

      } else
      {
       if(word.length() > 0 )
       {
       v.push_back(word);
       word = "";
       }
      }

    }
return v.size();

    }


unsigned long countLines()
	{
		return 1;
	}

	unsigned long countUnLines (const string& s, set<string>& wl)
	{
		wl.insert(s);
		return wl.size();
	}




int main()
{

//character count

  string words;

  unsigned long word_counter = 0;
	unsigned long Lines = 0;
	unsigned long UnLines = 0;
  unsigned long un_words;
  unsigned long character_counter = 0;

  vector<string> wv;
  set<string> ws;
	set<string> wl;
 while (getline(cin,words)){
 //unsigned long wordlist=0;
 //while(){

  //set<string> words;

  //countWords(words,wo);


  //w=0;<<w<<

     Lines += countLines();

		UnLines = countUnLines(words,wl);

    words += '\n';

    word_counter = wcounter(words,wv);

    character_counter +=  characters(words);

    un_words = uniq_words(words, ws);



}


 cout<<Lines << "\t" << word_counter <<"\t"<< character_counter <<'\t'<<UnLines<< "\t" << un_words<< endl;






	return 0;
}
