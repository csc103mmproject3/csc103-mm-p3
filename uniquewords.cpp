/*
 * CSc103 Project 3: wordcount++
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

// write this function to help you out with the computation.
// unsigned long countWords(const string& s, set<string>& wl);

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;


int main()
{
 cout<<"Enter Words " <<endl;
 string words;
 vector<string> stringHolder;

 while(cin>>words){
  stringholder.push_back(words);
 }

 sort(stringHolder.begin(),stringHolder.end());

 int vSize= stringHolder.size();

 int wordCount=1;
 words=stringHolder[0];


 for(int i=1;i<vSize;i++){
  if(words!=stringHolder[i]){
   cout<<words <<" appeared "<<wordCount <<" times " <<endl;
   wordCount=0;
   words=stringHolder[i];
  }
  wordCount++;
 }
 cout<<words<< " appeared " <<wordCount<<" times " <<endl;
 return 0;
}














